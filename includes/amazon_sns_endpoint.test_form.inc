<?php

/**
 * @file
 * Provides a simple test form to allow administrators to test that the
 * Amazon SNS Endpoint module can receive updates from Amazon SNS.
 */

/**
 * Form which provides a simple textarea with a keyName|keyValue on each line.
 *
 * @param array $form
 *   A form array.
 *
 * @param array $form_state
 *   The form state.
 */
function amazon_sns_endpoint_test_form($form, &$form_state) {
  drupal_set_title(t('Test the Amazon SNS Endpoint'));

  // Set the default textarea contents:
  if (isset($_SESSION['amazon_sns_endpoint_test_textarea_contents'])) {
    $textarea_contents = $_SESSION['amazon_sns_endpoint_test_textarea_contents'];
  }
  else {
    $textarea_contents = 'Type|Notification
TopicArn|arn:aws:sns:us-east-1:318514470594:WAYJ_NowPlaying_Test
MessageId|12345
Timestamp|' . REQUEST_TIME . '
Subject|Test message subject
SubscribeURL|
Message|This is a test message';
  }

  $form['blurb'] = array(
    '#markup' => t('<p>Use this form to compose and send a sample SNS notification to your site\'s Amazon SNS Endpoint.</p> <p>The textarea below contains one key and value per line, separated by a pipe symbol.</p> <p>Set the value of Type to either <em>Notification</em> or <em>SubscriptionConfirmation</em>.</p> <p>You can add more values in and change any existing ones as you wish.</p> <p>When you click "Send message", the data will be sent to your Amazon SNS endpoint endpoint.</p> <p>You can !watchdog_link for full details of how your message was processed.</p> <p>The SNS endpoint is currently located at: !endpoint_link</p> <p><strong>Note that this test will bypass source domain and certificate validation checks, but restrictions by topic will still be checked.</strong></p>', array(
      '!endpoint_link' => l(url(variable_get('amazon_sns_endpoint_path'), array('absolute' => TRUE)), variable_get('amazon_sns_endpoint_path')),
      '!watchdog_link' => l(t('check the Watchdog logs here'), 'admin/reports/dblog'),
    )),
  );

  $form['#attributes']['target'] = '_blank';

  $form['textarea'] = array(
    '#type' => 'textarea',
    '#title' => t('Test message'),
    '#description' => t('Enter one key and value per line, separated with a pipe, for example <em>TopicArn|arn:aws:sns:us-east-1:318514470594:WAYJ_NowPlaying_Test</em> would send a <em>TopicArn</em> variable with a value of <em>arn:aws:sns:us-east-1:318514470594:WAYJ_NowPlaying_Test</em> to the endpoint.'),
    '#default_value' => $textarea_contents,
    '#rows' => count(explode("\r\n", $textarea_contents)) + 2,
  );

  $form['submit_warning'] = array(
    '#markup' => t('<div class="messages warning">When you click "Send message", a new window will be opened to make the SNS call. Check the Watchdog logs for full details.</div>'),
  );

  $form['#validate'][] = 'amazon_sns_endpoint_test_form_validate';
  $form['#submit'][] = 'amazon_sns_endpoint_test_form_submit';

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send message'),
  );

  return $form;
}

/**
 * Validate handler for the Amazon SNS test form.
 *
 * Turn the textarea's contents into an array of keys/values, and add in the
 * IsTest and TestToken values which allow us to bypass the certificate and
 * source domain checks.
 *
 * @param array  $form
 *   A form array.
 *
 * @param array  $form_state
 *   The form state.
 */
function amazon_sns_endpoint_test_form_validate(&$form, &$form_state) {
  $keys_and_values_merged = explode("\r\n", trim($form_state['values']['textarea']));
  $keys_and_values_split = array();

  foreach ($keys_and_values_merged as $key_and_value) {
    list($key, $value) = explode('|', $key_and_value, 2);
    $keys_and_values_split[$key] = $value;
  }

  // Add in the test keys:
  $keys_and_values_split['IsTest'] = TRUE;
  $keys_and_values_split['TestToken'] = drupal_get_token('amazon_sns_endpoint_test');

  // Put the array back into the form_state so we can grab it in the submit
  // function:
  $form_state['values']['keys_and_values_split'] = $keys_and_values_split;

  // Also add the raw contents of the textarea into $_SESSION so we can pre-
  // populate the textarea when we return to it:
  $_SESSION['amazon_sns_endpoint_test_textarea_contents'] = $form_state['values']['textarea'];
}

/**
 * Submit handler for the SNS Endpoint test function.
 *
 * Displays form on the screen and uses JavaScript to issue a submit event to
 * post the form data back to the SNS endpoint callback.
 *
 * @param array  $form
 *   A form array.
 *
 * @param array  $form_state
 *   The form state.
 */
function amazon_sns_endpoint_test_form_submit(&$form, &$form_state) {
  $json_data = json_encode($form_state['values']['keys_and_values_split']);
  $url = url(variable_get('amazon_sns_endpoint_path'), array('absolute' => TRUE));

  $curl_handle = curl_init();

  curl_setopt($curl_handle, CURLOPT_URL, $url);
  curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($curl_handle, CURLOPT_POST, TRUE);
  curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $json_data);
  curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array('Content-Type: text/plain'));

  $result = curl_exec($curl_handle);

  echo $result;

  drupal_exit();
}
