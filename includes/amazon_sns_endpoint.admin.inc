<?php

/**
 * @file
 * Provides the administration settings form for the Amazon SNS Endpoint
 * module.
 */

/**
 * Administration settings form for the Amazon SNS Endpoint module.
 *
 * @param array $form
 *   A form array.
 *
 * @param array $form_state
 *   The form state.
 */
function amazon_sns_endpoint_admin_form($form, &$form_state) {
  $form['general_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
  );

  $form['amazon_sns_endpoint_path_original'] = array(
    '#type' => 'value',
    '#value' => variable_get('amazon_sns_endpoint_path'),
  );

  $form['general_settings']['amazon_sns_endpoint_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Endpoint path'),
    '#description' => t('Provide a path (without leading or trailing slashes) which the Amazon SNS Endpoint module should listen on. The endpoint is currently listening at !endpoint_link.', array(
      '!endpoint_link' => l(url(variable_get('amazon_sns_endpoint_path'), array('absolute' => TRUE)), url(variable_get('amazon_sns_endpoint_path'))),
    )),
    '#field_prefix' => url('<front>', array('absolute' => TRUE)),
    '#default_value' => variable_get('amazon_sns_endpoint_path'),
    '#required' => TRUE,
  );

  $form['security_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Security settings'),
  );

  $form['security_settings']['amazon_sns_endpoint_allowed_topic'] = array(
    '#type' => 'textfield',
    '#title' => t('Allowed SNS Topic'),
    '#description' => t('Check that your messages are coming from the correct topicArn, e.g. arn:aws:sns:us-east-1:318514470594:WAYJ_NowPlaying_Test'),
    '#default_value' => variable_get('amazon_sns_endpoint_allowed_topic'),
  );

  $form['security_settings']['amazon_sns_endpoint_verify_certificate'] = array(
    '#type' => 'checkbox',
    '#title' => t('Verify server certificate?'),
    '#description' => t('For security you can (should) validate the certificate, this does add an additional time demand on the system. NOTE: This also checks the origin of the certificate to ensure messages are signed by the AWS SNS SERVICE. Since the allowed topicArn is part of the validation data, this ensures that your request originated from the service, not somewhere else, and is from the topic you think it is, not something spoofed. e.g. sns.us-east-1.amazonaws.com'),
    '#default_value' => variable_get('amazon_sns_endpoint_verify_certificate'),
  );

  $form['security_settings']['amazon_sns_endpoint_source_domain_to_validate'] = array(
    '#type' => 'textfield',
    '#title' => t('Only accept notifications from this domain.'),
    '#description' => t('For example <em>www.example.com</em>. Don\'t prefix with "http://" or add a trailing slash. Leave this blank to allow notifications from any domain.'),
    '#default_value' => variable_get('amazon_sns_endpoint_source_domain_to_validate'),
    '#field_prefix' => 'http://',
    '#field_suffix' => '/',
  );

  $form['#submit'][] = 'amazon_sns_endpoint_admin_form_submit';

  return system_settings_form($form);
}

/**
 * Submit handler for the amazon_sns_endpoint_admin_form function.
 *
 * Initiates a menu rebuild if the endpoint path has changed.
 *
 * @param array  $form
 *   A form array.
 *
 * @param array  $form_state
 *   The form state.
 */
function amazon_sns_endpoint_admin_form_submit(&$form, &$form_state) {
  if ($form_state['values']['amazon_sns_endpoint_path_original'] != $form_state['values']['amazon_sns_endpoint_path']) {
    menu_router_build();
    menu_cache_clear_all();
  }

  // Remove "amazon_sns_endpoint_path_original"; we don't want to save it as a
  // Drupal variable:
  unset($form_state['values']['amazon_sns_endpoint_path_original']);
}
