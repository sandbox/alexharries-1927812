About this module
=================

Overview

This is a very simple module which creates an endpoint which the Amazon Simple
Notifications Service can be configured to send messages to.

I built this module recently as part of a website which is using Amazon's Simple
Email Service and, as part of that service's setup we had to provide a way of
allowing Amazon to send notifications from the e-mail service back to our
website; for example, to record e-mail bounces or spam reports.

Messages received from Amazon SNS are logged to the Drupal Watchdog for later
review (I'm open to suggestions on alternative ways of storing the notifications
- for example creating a separate table and providing the messages as
Views-friendly entities, but this was out of scope for the project I used this
code in).

This module has been built around code from
https://github.com/npflood/AWS-SNS-HTTP-PHP-ENDPOINT with thanks.

Requirements

- A configured Amazon SNS subscription - http://aws.amazon.com/sns/
- cURL on your server - http://php.net/manual/en/book.curl.php

Configuration

- Configure Amazon SNS and choose an endpoint URL on your site to send
  notifications to, e.g. www.example.com/amazonsnsendpoint
- Install and enable this module
- If installing via Drush, you may also need to perform a "drush cc all" command
  to enable the admin page
- Go to admin/config/services/amazonsnsendpoint and configure the options to
  suit your site (hopefully these should be self-explanatory - examples are
  provided as appropriate)

Testing

To simulate a message being sent to your site, go to
admin/config/services/amazonsnsendpoint/test where you can find a simple form
which lets you compose and send a sample message.
